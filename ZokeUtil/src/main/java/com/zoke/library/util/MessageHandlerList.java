package com.zoke.library.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

/**
 * @author 大熊
 * @version 1.0
 * @fileName MessageHandlerList.java
 * @description 统一管理Handler
 * @email 651319154@qq.com
 */
public class MessageHandlerList {
    private MessageHandlerList() {
    }

    private static class MessageHandlerListLoader {
        private static final MessageHandlerList INSTANCE = new MessageHandlerList();
    }

    public static MessageHandlerList getInstance() {
        return MessageHandlerListLoader.INSTANCE;
    }

    private Map<String, Handler> mMap = new HashMap<>();

    /**
     * 根据键名添加handler到消息池
     *
     * @param className
     * @param handler
     */
    public static synchronized void addHandler(String className, Handler handler) {
        if (getInstance().mMap.get(className) == null) {
            getInstance().mMap.put(className, handler);
        }
    }

    /**
     * 根据类名将对应的handler移除消息池
     *
     * @param className
     */
    public static synchronized void removeHandler(String className) {
        getInstance().mMap.remove(className);
    }

    /**
     * 根据指定Handler发送消息
     *
     * @param handler
     * @param msgId
     * @param msgObj
     */
    public static synchronized void sendMessage(Handler handler, int msgId,
                                                Object msgObj) {
        Message message = handler.obtainMessage();
        message.what = msgId;
        message.obj = msgObj;
        handler.sendMessage(message);
    }

    /**
     * 全局发送消息
     *
     * @param msgId
     * @param msgObj
     */
    public synchronized static void sendMessage(int msgId, Object msgObj) {
        synchronized (getInstance().mMap) {
            for (Entry<String, Handler> entry : getInstance().mMap.entrySet()) {
                Handler handler = entry.getValue();
                sendMessage(handler, msgId, msgObj);
            }
        }
    }

    /**
     * 发送空消息
     *
     * @param msgId
     */
    public static void sendMessage(int msgId) {
        sendMessage(msgId, null);
    }

    /**
     * 根据指定类名发送消息
     *
     * @param className 类名
     * @param msgId
     * @param msgObj
     */
    public static void sendMessage(String className, int msgId, Object msgObj) {
        Handler handler = getInstance().mMap.get(className);
        if (handler != null) {
            sendMessage(handler, msgId, msgObj);
        }
    }

    /**
     * 根据类名获取对应activity的Handler
     *
     * @return
     * @method
     */
    public static Handler getHandler(@SuppressWarnings("rawtypes") Class cls) {
        return getInstance().mMap.get(cls.getName());
    }

    /**
     * 根据类名发送消息
     *
     * @param cls
     * @param msgId
     * @param msgObj
     */
    public static void sendMessage(@SuppressWarnings("rawtypes") Class cls,
                                   int msgId, Object msgObj) {
        sendMessage(cls.getName(), msgId, msgObj);
    }

    /**
     * 根据类名发送空消息
     *
     * @param cls
     * @param msgId
     */
    public static void sendMessage(@SuppressWarnings("rawtypes") Class cls,
                                   int msgId) {
        sendMessage(cls.getName(), msgId, null);
    }
}
