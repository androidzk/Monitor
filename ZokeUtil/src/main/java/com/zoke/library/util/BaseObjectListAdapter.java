package com.zoke.library.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * 适配器基类
 *
 * @author 大熊
 */
public abstract class BaseObjectListAdapter<T> extends BaseAdapter {

    protected Context mContext;
    protected LayoutInflater mInflater;
    protected List<T> mDatas;// 数据集合

    public BaseObjectListAdapter(Context context, List<T> datas) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        if (datas != null) {
            mDatas = datas;
        }
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getItemView(position, convertView, parent);
    }

    /**
     * 子类必须实现的处理getView的方法
     **/
    public abstract View getItemView(int position, View convertView,
                                     ViewGroup parent);

    public List<T> getDatas() {
        return mDatas;
    }

    /**
     * 获取convertView
     **/
    protected View getConvertView(View convertView, int resid) {
        if (null == convertView) {
            convertView = mInflater.inflate(resid, null);
        }
        return convertView;
    }
}
