package com.zoke.monitor.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zoke.library.util.ViewHolderUtils;
import com.zoke.monitor.R;

import java.util.List;

/**
 * @author JackWu
 */
public class TabBottomAdapter extends BaseIndicatorAdapter {
    public TabBottomAdapter(List<? extends Object> list, Context context) {
        super(list, context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = getConvertView(convertView, R.layout.tab_item_navbottom);
        TabBottom tb = (TabBottom) mDatas.get(position);
        ImageView tabiv = ViewHolderUtils.get(convertView, R.id.tab_icon_iv);
        TextView tabtv = ViewHolderUtils.get(convertView, R.id.tab_text_tv);
        tabiv.setBackgroundResource(tb.iconRes);
        tabtv.setText(tb.tabName);
        return convertView;
    }
}
