package com.zoke.monitor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoke.monitor.view.Indicator;

import java.util.List;

public class BaseIndicatorAdapter extends Indicator.IndicatorAdapter {

    public List<? extends Object> mDatas;
    public Context mContext;
    public LayoutInflater mInflater;

    public BaseIndicatorAdapter(List<? extends Object> list, Context context) {
        this.mDatas = list;
        this.mContext = context;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    protected View getConvertView(View convertView, int resId) {
        if (convertView == null)
            convertView = mInflater.inflate(resId, null);
        return convertView;
    }

}
