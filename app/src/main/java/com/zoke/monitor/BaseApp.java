package com.zoke.monitor;

import android.app.Application;

import org.xutils.x;

/**
 * Created by bear on 16/5/30.
 */

public class BaseApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);
        x.Ext.setDebug(BuildConfig.DEBUG); // 是否输出debug日志, 开启debug会影响性能.
    }
}
