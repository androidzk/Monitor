package com.zoke.monitor.fragment;

import android.nfc.NdefMessage;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.widget.TextView;

import com.zoke.monitor.R;
import com.zoke.monitor.nfc.NdefMessageParser;
import com.zoke.monitor.nfc.record.ParsedNdefRecord;

import org.xutils.common.util.LogUtil;
import org.xutils.view.annotation.ViewInject;

import java.util.List;

/**
 * Created by bear on 16/5/30.
 */
public class NFCFragment extends BaseFragment {
    @ViewInject(R.id.tag)
    private TextView tag;

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        super.onCreateView(savedInstanceState);
        setContentView(R.layout.fragment_nfc);
    }

    @Override
    protected void handleMsg(Message msg) {
        super.handleMsg(msg);
        switch (msg.what) {
            case 100:
                NdefMessage[] msgs = (NdefMessage[]) msg.obj;
                if (msgs == null || msgs.length == 0) {
                    return;
                }
                List<ParsedNdefRecord> records = NdefMessageParser.parse(msgs[0]);
                final int size = records.size();
                for (int i = 0; i < size; i++) {
                    ParsedNdefRecord record = records.get(i);
                    String cardInfo = record.getNdfContent().trim();
                    if (!TextUtils.isEmpty(cardInfo)) {
                        LogUtil.e(cardInfo);
                        if (cardInfo.equals("id001")) {
                            //管道1
                            tag.setText("管道编号001");
                        } else if (cardInfo.equals("id002")) {
                            //管道1
                            tag.setText("管道编号002");
                        } else {
                            tag.setText(cardInfo);
                        }
                    }

                }
                break;
            case 101:
                tag.setText("该机型不支持NFC，请使用支持NFC模块的手机");
                break;
        }
    }
}
