package com.zoke.monitor.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoke.library.util.MessageHandlerList;
import com.zoke.monitor.BaseActivity;

import org.xutils.common.util.LogUtil;
import org.xutils.x;


/**
 * 支持MessageHandlerList的Fragment 统一管理网络请求
 * ps:从Fragment跳转到Activity，将Activity的主题设置为fragment_activity。
 *
 * @author JackWu
 */
public class BaseFragment extends Fragment {
    protected Handler mHandler;

    private LayoutInflater inflater;
    private View contentView;
    private ViewGroup container;

    /**
     * Fragment当前状态是否可见
     */
    protected boolean isVisible;
    protected boolean isCreate = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 设置MessageHandlerList
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                handleMsg(msg);
            }
        };
        String className = this.getClass().getName();
        MessageHandlerList.addHandler(className, mHandler);
    }

    protected void handleMsg(Message msg) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isCreate = false;
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }


    /**
     * 当Fragment可见时调用该方法
     */
    protected void onVisible() {
        LogUtil.e("Fragment onVisible");
        lazyLoad();//只有在可见 并且初始化已经完毕的时候调用
    }

    /**
     * Fragment不可见时调用
     */
    protected void onInvisible() {
        LogUtil.e("Fragment onInvisible");
    }

    protected void lazyLoad() {

    }

    /**
     * 获取Handle
     **/
    protected Handler getHandler() {
        return mHandler;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        onCreateView(savedInstanceState);
        //初始化Xutils
        if (contentView == null)
            return super.onCreateView(inflater, container, savedInstanceState);
        return contentView;
    }

    /**
     * 子类实现该方法 简化fragment添加View的方法 等同于fragment的onCreateView
     **/
    protected void onCreateView(Bundle savedInstanceState) {
        LogUtil.e("Fragment onCreateView");
    }

    /**
     * 模仿Activity查找控件
     **/
    public View findViewById(int id) {
        return contentView.findViewById(id);
    }


    @Override
    public void onDestroy() {
        // 移除消息池中对应的handler
        String className = this.getClass().getName();
        MessageHandlerList.removeHandler(className);
        super.onDestroy();
        contentView = null;
        container = null;
        inflater = null;

    }


    /**
     * 仿照Acitivity添加布局的方式
     **/
    public void setContentView(int layoutResID) {
        setContentView(inflater.inflate(layoutResID, container, false));
        x.view().inject(this, getContentView());
    }

    public void setContentView(View view) {
        contentView = view;
    }

    public View getContentView() {
        return contentView;
    }


}
